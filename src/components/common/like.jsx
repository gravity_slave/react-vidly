import React from "react";

const Like = ({ movie, onToggleLike }) => {
  let classes = "fa fa-heart";
  if (!movie.liked) classes += "-o";
  return (
    <i
      className={classes}
      aria-hidden="true"
      onClick={() => onToggleLike(movie)}
      style={{ cursor: "pointer" }}
    />
  );
};

export default Like;
