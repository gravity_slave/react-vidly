import React from "react";

const ListGroup = ({
  items: genres,
  textProperty: label,
  valueProperty: value,
  onItemSelect,
  selectedItem
}) => {
  return (
    <ul className="list-group">
      {genres.map( (genre, index) => (
        <li
          className={
            genre === selectedItem
              ? "list-group-item active"
              : "list-group-item"
          }
          key={index}
          onClick={() => onItemSelect(genre)}
        >
          {genre[label]}
        </li>
      ))}
    </ul>
  );
};

ListGroup.defaultProps = {
  textProperty: "name",
  valueProperty: "_id"
};

export default ListGroup;
