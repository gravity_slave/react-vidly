import React, { Component } from "react";
import * as _ from "lodash";

class TableBody extends Component {
  renderCell = (item, col) => {
    if (col.content) return col.content(item);
    return _.get(item, col.path);
  };
  render() {
    const { movies, columns } = this.props;
    return (
      <tbody>
        {movies.map((movie, index) => (
          <tr key={index}>
            {columns.map((col, inx) => (
              <td key={inx}>{this.renderCell(movie, col)}</td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  }
}

export default TableBody;
