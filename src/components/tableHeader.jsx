import React, { Component } from "react";

class TableHeader extends Component {
  raiseSort = path => {
    const sortColumn = { ...this.props.sortColumn };
    if (sortColumn.path === path) {
      sortColumn.order = sortColumn.order === "asc" ? "desc" : "asc";
    } else {
      sortColumn.order = "asc";
      sortColumn.path = path;
    }
    this.props.onSort(sortColumn);
  };

  renderSortIcon = (col) => {
    const { sortColumn } = this.props;
    if (col.path !== sortColumn.path) return null;
    if (sortColumn.order === 'asc')
      return <i className='fa fa-sort-asc'></i>;
    else
      return <i className='fa fa-sort-desc'></i>;
  };
  render() {
    return (
      <thead>
        <tr>
          {this.props.columns.map((col, index) => (
            <th key={index} onClick={() => this.raiseSort(col.path)}>
              {col.label} {this.renderSortIcon(col)}
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
